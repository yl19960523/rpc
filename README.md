# RPC的简介和原理       
>1. 为什么？    
一个系统由多个模块组成，随着业务的增长。我们回去更改系统的业务架构。一般是以功能去拆分，每个功能当作一个应用去维护。如下图：
![avatar](微服务.jpg) 
但是拆分之后，会出现问题。比如我现在下了一单，我现在要去支付，我就需要去请求支付的应用模块，这个请求我们就可以理解为RPC。


>2. 分布式部署的调用           
当我们将系统拆分成多个应用之后就变成了分布式的部署方式。不同的应用会被部署在不同的服务器上。RPC(remote procedure call protocol)翻译成中文就是远程过程调用协议。
![avatar](分布式部署.jpg)
特点：这种调用跨越了物理服务器的限制，在网络中完成，在调用远程服务器上程序的过程中本地的服务器等待返回调用结果，知道远程程序执行完毕，将结果进行返回到本地，最终完成一次完整的调用。

>3. RPC技术架构         
客户端：服务调用发起方，又称之为服务消费者。       
服务器：远端计算机机器上运行的程序，其中包含客户端要调用和访问的方法        
客户端存根：存放服务端的地址，端口消息。将客户端的请求参数打包成网络消息，发送到服务方。接受服务方放回的数据包。该程序运行在客户端。        
服务端存梗：接受客户端发送的数据包，解析数据包，调用具体的服务方法。将调用结果打包发送给客户端一方。该段程序运行在服务端。
![avatar](RPC原理.jpg)
存根

>4. go中RPC的实现     
```go
package main

import (
	"net/rpc"
	"fmt"
)

//客户端逻辑实现
func main() {

	client, err := rpc.DialHTTP("tcp", "localhost:8081")
	if err != nil {
		panic(err.Error())
	}

	var req float32 //请求值
	req = 5

	//var resp *float32 //返回值
	//同步的调用方式,MathUtil表示调用服务端的对象，CalculateCircleArea表示该对象下的方法
	//err = client.Call("MathUtil.CalculateCircleArea", req, &resp)
	//if err != nil {
	//	panic(err.Error())
	//}
	//fmt.Println(*resp)

	var respSync *float32
	//异步的调用方式
	syncCall := client.Go("MathUtil.CalculateCircleArea", req, &respSync, nil)
	//fmt.Println(*respSync)
	replayDone := <-syncCall.Done
	fmt.Println(replayDone)
	fmt.Println(*respSync)
}
```

```go
package main

import (
	"math"
	"net/rpc"
	"net"
	"net/http"
)

//数学计算
type MathUtil struct {
}

//该方法向外暴露：提供计算圆形面积的服务
func (mu *MathUtil) CalculateCircleArea(req float32, resp *float32) error {
	*resp = math.Pi * req * req //圆形的面积 s = π * r * r
	return nil                  //返回类型
}

//main 方法
func main() {

	//1、初始化指针数据类型
	mathUtil := new(MathUtil) //初始化指针数据类型

	//2、调用net/rpc包的功能将服务对象进行注册
	err := rpc.Register(mathUtil)
	if err != nil {
		panic(err.Error())
	}

	//3、通过该函数把mathUtil中提供的服务注册到HTTP协议上，方便调用者可以利用http的方式进行数据传递
	rpc.HandleHTTP()

	//4、在特定的端口进行监听
	listen, err := net.Listen("tcp", ":8081")
	if err != nil {
		panic(err.Error())
	}
	http.Serve(listen, nil)
}
```

>5. protobuf        
1）简介：       
protocol buffers是一种轻便高效的结构化数据存储格式，可以用于结构化数据串行化，或者说系列化。他很适合数据存储或者RPC数据交换格式。可用于通讯协议，数据存储等领域的语言无关，平台无关，可扩展的序列化结构数据格式。简单来说protobuf的功能类似于XML，即负责把某种数据结构的信息，以某种格式存储起来。平时常见的还有XML，JSON等已经很普遍的数据传输方式。   
2）优点：       
时间维度：速度快    
空间维度：消耗少

3）定义：
```go
//message.proto文件
message Person {
    required string Name = 1;
    required int32 Age = 2;
    required string From = 3;
    optional Address Addr = 4;
    message Address {
        required sint32 id = 1 [default = 1];
        required string name = 2 [default = '北京'];
        optional string pinyin = 3 [default = 'beijing'];
        required string address = 4;
        required bool flag = 5 [default = true];
    }
}
```
使用指令protoc ./message.proto --go_out = ./   执行之后生成
```go
//message.pb.go文件
// Code generated by protoc-gen-go. DO NOT EDIT.
// source: person.proto

package example

import (
	fmt "fmt"
	proto "github.com/golang/protobuf/proto"
	math "math"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion2 // please upgrade the proto package

type Person struct {
	Name                 *string  `protobuf:"bytes,1,req,name=Name" json:"Name,omitempty"`
	Age                  *int32   `protobuf:"varint,2,req,name=Age" json:"Age,omitempty"`
	From                 *string  `protobuf:"bytes,3,req,name=From" json:"From,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *Person) Reset()         { *m = Person{} }
func (m *Person) String() string { return proto.CompactTextString(m) }
func (*Person) ProtoMessage()    {}
func (*Person) Descriptor() ([]byte, []int) {
	return fileDescriptor_4c9e10cf24b1156d, []int{0}
}

func (m *Person) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_Person.Unmarshal(m, b)
}
func (m *Person) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_Person.Marshal(b, m, deterministic)
}
func (m *Person) XXX_Merge(src proto.Message) {
	xxx_messageInfo_Person.Merge(m, src)
}
func (m *Person) XXX_Size() int {
	return xxx_messageInfo_Person.Size(m)
}
func (m *Person) XXX_DiscardUnknown() {
	xxx_messageInfo_Person.DiscardUnknown(m)
}

var xxx_messageInfo_Person proto.InternalMessageInfo

func (m *Person) GetName() string {
	if m != nil && m.Name != nil {
		return *m.Name
	}
	return ""
}

func (m *Person) GetAge() int32 {
	if m != nil && m.Age != nil {
		return *m.Age
	}
	return 0
}

func (m *Person) GetFrom() string {
	if m != nil && m.From != nil {
		return *m.From
	}
	return ""
}

func init() {
	proto.RegisterType((*Person)(nil), "example.Person")
}

func init() { proto.RegisterFile("person.proto", fileDescriptor_4c9e10cf24b1156d) }

var fileDescriptor_4c9e10cf24b1156d = []byte{
	// 98 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0xe2, 0xe2, 0x29, 0x48, 0x2d, 0x2a,
	0xce, 0xcf, 0xd3, 0x2b, 0x28, 0xca, 0x2f, 0xc9, 0x17, 0x62, 0x4f, 0xad, 0x48, 0xcc, 0x2d, 0xc8,
	0x49, 0x55, 0x72, 0xe2, 0x62, 0x0b, 0x00, 0x4b, 0x08, 0x09, 0x71, 0xb1, 0xf8, 0x25, 0xe6, 0xa6,
	0x4a, 0x30, 0x2a, 0x30, 0x69, 0x70, 0x06, 0x81, 0xd9, 0x42, 0x02, 0x5c, 0xcc, 0x8e, 0xe9, 0xa9,
	0x12, 0x4c, 0x0a, 0x4c, 0x1a, 0xac, 0x41, 0x20, 0x26, 0x48, 0x95, 0x5b, 0x51, 0x7e, 0xae, 0x04,
	0x33, 0x44, 0x15, 0x88, 0x0d, 0x08, 0x00, 0x00, 0xff, 0xff, 0xef, 0x7e, 0x45, 0x81, 0x5b, 0x00,
	0x00, 0x00,
}

```

6. gRpc         
    grpc是一款高性能的远程过程调用(RPC)框架，在任何环境下运行，该框架提供了负载均衡，跟踪，智能监控，身份验证等功能，可以实现系统间的高效链接。grpc框架默认使用protocol buffers作为接口语言，用于描述网络传输消息结构。除此之外还可以使用protobuf定义服务接口

    1)协议缓冲区    
    默认情况下，gRPC使用协议缓冲区，用于序列化结构化数据(尽管他可以与其他数据格式(例如json)一起使用)。协议缓冲区是一种用于序列化结构化数据的灵活，高效，自动化的机制。您定义要构造数据的方式，然后可以使用生成的特殊源代码轻松的使用各种语言在各种数据流中写入和读取结构化数据。
    2)他们如何工作？        
    你可以通过.proto文件来定义协议缓冲区消息类型来指定要序列化的信息的结构。每个协议缓冲区消息都是一个小的逻辑信息记录，其中包含一系列key/value对。
    ```go
    message Person {
    required string name = 1;
    required int32 id = 2;
    optional string email = 3;

    enum PhoneType {
        MOBILE = 0;
        HOME = 1;
        WORK = 2;
    }

    message PhoneNumber {
        required string number = 1;
        optional PhoneType type = 2 [default = HOME];
    }

    repeated PhoneNumber phone = 4;
    }
    ```
    一旦你定义了.proto文件中这样的消息结构。你可以就可以为你的应用语言生成对应的访问类。    
    3）gRPC可以定义四种服务方法：       
    ～一元RPC，客户端向服务器发送当个请求并获得响应，就像普通函数的调用一样。
    >rpc SayHello(HelloRequest) returns (HelloResponse){}
    
    ～服务器流式RPC，客户端在其中向服务器发送请求，并获取流以读取一系列消息。客户端从返回的流中读取，直到没有更多消息。grpc保证单个rpc调用的消息顺序。
    >rpc LotsOfReplies(HelloRequest) returns (stream HelloResponse){}       

    ~客户端流式，客户端在其中编写消息序列，然后在次使用提供的流将他们发送到服务器。客户端写完消息后，他将等待服务器读取消息并返回响应。gRPC再次保证了在单个RPC调用中的消息顺序。
    >rpc LotsOfGreetings(stream HelloRequest) returns (HelloResponse) {}   

    ~双向流式RPC，双方都使用读写流发送一系列消息。这两个流是独立运行的，因此客户端和服务器可以按照自己喜欢的顺序进行读写：例如，服务器可以在写响应之前等待接收所有客户端消息，或者可以先读取消息再写入消息，或其他一些读写组合。每个流中的消息顺序都会保留。
    >rpc BidiHello(stream HelloRequest) returns (stream HelloResponse){}
